from PyQt5 import QtCore
from PyQt5.QtWidgets import *
from PyQt5 import QtWebKitWidgets

class Ui_MainWindow(object):
    def __init__(self, MainWindow):
        super().__init__()
        self.initUI(MainWindow)

    def goClick(self):
        self.page.setUrl(QtCore.QUrl(self.url.text()))

    def initUI(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowTitle(_translate("Web Browser", "Web Browser"))
        MainWindow.resize(660, 660)

        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.url = QLineEdit(self.centralwidget)
        self.url.setEchoMode(QLineEdit.Normal)
        self.url.setGeometry(QtCore.QRect(10, 10, 551, 31))
        self.url.setText("http://w1tch.iptime.org:8090/")

        self.go = QPushButton(self.centralwidget)
        self.go.setGeometry(QtCore.QRect(560, 10, 75, 31))
        self.go.setText(_translate("MainWindow", "go"))
        self.go.clicked.connect(self.goClick)

        self.page = QtWebKitWidgets.QWebView(self.centralwidget)
        self.page.setGeometry(QtCore.QRect(10, 60, 621, 371))
        self.page.setUrl(QtCore.QUrl(self.url.text()))

        MainWindow.setCentralWidget(self.centralwidget)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)